package com.steiner.comparator.stores.rozetka;


import com.steiner.comparator.Config;
import com.steiner.comparator.SearchEngine;
import com.steiner.comparator.stores.BaseStore;
import com.steiner.comparator.stores.rozetka.entity.CommodityR;
import com.steiner.comparator.stores.rozetka.entity.Goods;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.List;

public class Rozetka extends BaseStore {

    public Rozetka(String request) {
        super(Config.ROZETKA_API_URL, request);
    }

    public static TypedInput makeInput(String request) {
        try {
            return new TypedByteArray("application/x-www-form-urlencoded; charset=UTF-8", request.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public RozetkaService service() {
        return getRestAdapter().create(RozetkaService.class);
    }

    @Override
    public void run() {
        super.run();
        this.setIsDebug(Config.IS_DEBUG);
        RozetkaService rozetkaService = this.service();
        Goods goods = rozetkaService.findGoods(Rozetka.makeInput("text=" + this.request + "&suggest=1"));
        List<CommodityR> commodities = goods.getContent().getCommodities();

        if (commodities.size() > 0) {
            for (int i = 0; i < commodities.size(); i++) {
                commodities.get(i).setPrice(commodities.get(i).getPrice().replace("&nbsp;", "").replace("грн.", ""));
            }
            Collections.sort(commodities);

            System.out.println("Топ Rozetka: ");
            if (commodities.size() > 3) {
                for (int i = 0; i < 3; i++) {
                    System.out.println("Rozetka: " + commodities.get(i).getPrice() + " -- " + commodities.get(i).getTitle());
                }
            } else {
                for (CommodityR commodity : commodities) {
                    System.out.println("Rozetka: " + commodity.getPrice() + " -- " + commodity.getTitle());
                }
            }
            SearchEngine.rozetkaLow = commodities.get(0);
            /*for (Commodity commodity : commodities) {
                System.out.println("Rozetka: " + commodity.getPrice() + " -- " + commodity.getTitle());
            }*/
        }

    }
}
