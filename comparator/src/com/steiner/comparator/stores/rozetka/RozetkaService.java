package com.steiner.comparator.stores.rozetka;

import com.steiner.comparator.stores.rozetka.entity.Goods;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.mime.TypedInput;

public interface RozetkaService {
    @Headers({
            "Host: rozetka.com.ua",
            "Connection: keep-alive",
            "Content-Length: 19",
            "Origin: http://rozetka.com.ua",
            "ajaxAction: http://rozetka.com.ua/search/",
            "User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.115 Safari/537.36",
            "Content-type: application/x-www-form-urlencoded; charset=UTF-8",
            "Accept: text/javascript, text/html, application/xml, text/xml, */*",
            "X-Requested-With: XMLHttpRequest",
            "Referer: http://rozetka.com.ua/",
            "Accept-Encoding: gzip, deflate",
            "Accept-Language: ru,en-US;q=0.8,en;q=0.6,uk;q=0.4",
            "Cookie: uid=WbhRglTzku5Vt3reAwMWAg==; device_type=computer; partner_id=1; href=http%3A%2F%2Frozetka.com.ua%2F; ab_search_abtests_id=10; ab_test=olddesign; ab_percent=20"
    })
    @POST("/cgi-bin/search-form.php")
    Response findGoodsRaw(@Body TypedInput in);

    @Headers({
            "Host: rozetka.com.ua",
            "Connection: keep-alive",
            "Content-Length: 19",
            "Origin: http://rozetka.com.ua",
            "ajaxAction: http://rozetka.com.ua/search/",
            "User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.115 Safari/537.36",
            "Content-type: application/x-www-form-urlencoded; charset=UTF-8",
            "Accept: text/javascript, text/html, application/xml, text/xml, */*",
            "X-Requested-With: XMLHttpRequest",
            "Referer: http://rozetka.com.ua/",
            "Accept-Encoding: gzip, deflate",
            "Accept-Language: ru,en-US;q=0.8,en;q=0.6,uk;q=0.4",
            "Cookie: uid=WbhRglTzku5Vt3reAwMWAg==; device_type=computer; partner_id=1"
    })
    @POST("/cgi-bin/search-form.php")
    Goods findGoods(@Body TypedInput in);
}
