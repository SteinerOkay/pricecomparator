package com.steiner.comparator.stores.rozetka.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class Content {

    @Expose
    private String text;
    @Expose
    @SerializedName("records")
    private List<CommodityR> commodities = new ArrayList<CommodityR>();
    @Expose
    private Integer count;
    @SerializedName("total_count")
    @Expose
    private Integer totalCount;
    @Expose
    private Integer page;
    @Expose
    private Integer start;
    @SerializedName("sections_menu")
    @Expose
    private List<Object> sectionsMenu = new ArrayList<Object>();

    /**
     * @return The text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text The text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return The records
     */
    public List<CommodityR> getCommodities() {
        return commodities;
    }

    /**
     * @param commodities The records
     */
    public void setCommodities(List<CommodityR> commodities) {
        this.commodities = commodities;
    }

    /**
     * @return The count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * @param count The count
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     * @return The totalCount
     */
    public Integer getTotalCount() {
        return totalCount;
    }

    /**
     * @param totalCount The total_count
     */
    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    /**
     * @return The page
     */
    public Integer getPage() {
        return page;
    }

    /**
     * @param page The page
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * @return The start
     */
    public Integer getStart() {
        return start;
    }

    /**
     * @param start The start
     */
    public void setStart(Integer start) {
        this.start = start;
    }

    /**
     * @return The sectionsMenu
     */
    public List<Object> getSectionsMenu() {
        return sectionsMenu;
    }

    /**
     * @param sectionsMenu The sections_menu
     */
    public void setSectionsMenu(List<Object> sectionsMenu) {
        this.sectionsMenu = sectionsMenu;
    }

}