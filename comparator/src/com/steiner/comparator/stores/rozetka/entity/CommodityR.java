package com.steiner.comparator.stores.rozetka.entity;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class CommodityR implements Comparable<CommodityR> {

    @Expose
    private String title;
    @Expose
    private String href;
    @Expose
    private String image;
    @Expose
    private String price;
    @SerializedName("price_usd")
    @Expose
    private String priceUsd;
    @SerializedName("sell_status")
    @Expose
    private String sellStatus;
    @Expose
    private String parent;
    @Expose
    private String producer;
    @Expose
    private String id;
    @SerializedName("price_usd_clear")
    @Expose
    private String priceUsdClear;
    @Expose
    private String test;
    @Expose
    private String list;
    @Expose
    private Integer position;
    @SerializedName("parent_id")
    @Expose
    private Integer parentId;
    @SerializedName("top_parent_id")
    @Expose
    private String topParentId;
    @SerializedName("producer_id")
    @Expose
    private Integer producerId;
    @Expose
    private Double rank;
    @Expose
    private String type;
    @Expose
    private String part;

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The href
     */
    public String getHref() {
        return href;
    }

    /**
     * @param href The href
     */
    public void setHref(String href) {
        this.href = href;
    }

    /**
     * @return The image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * @return The price
     */
    public String getPrice() {
        return price;
    }

    /**
     * @param price The price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * @return The priceUsd
     */
    public String getPriceUsd() {
        return priceUsd;
    }

    /**
     * @param priceUsd The price_usd
     */
    public void setPriceUsd(String priceUsd) {
        this.priceUsd = priceUsd;
    }

    /**
     * @return The sellStatus
     */
    public String getSellStatus() {
        return sellStatus;
    }

    /**
     * @param sellStatus The sell_status
     */
    public void setSellStatus(String sellStatus) {
        this.sellStatus = sellStatus;
    }

    /**
     * @return The parent
     */
    public String getParent() {
        return parent;
    }

    /**
     * @param parent The parent
     */
    public void setParent(String parent) {
        this.parent = parent;
    }

    /**
     * @return The producer
     */
    public String getProducer() {
        return producer;
    }

    /**
     * @param producer The producer
     */
    public void setProducer(String producer) {
        this.producer = producer;
    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The priceUsdClear
     */
    public String getPriceUsdClear() {
        return priceUsdClear;
    }

    /**
     * @param priceUsdClear The price_usd_clear
     */
    public void setPriceUsdClear(String priceUsdClear) {
        this.priceUsdClear = priceUsdClear;
    }

    /**
     * @return The test
     */
    public String getTest() {
        return test;
    }

    /**
     * @param test The test
     */
    public void setTest(String test) {
        this.test = test;
    }

    /**
     * @return The list
     */
    public String getList() {
        return list;
    }

    /**
     * @param list The list
     */
    public void setList(String list) {
        this.list = list;
    }

    /**
     * @return The position
     */
    public Integer getPosition() {
        return position;
    }

    /**
     * @param position The position
     */
    public void setPosition(Integer position) {
        this.position = position;
    }

    /**
     * @return The parentId
     */
    public Integer getParentId() {
        return parentId;
    }

    /**
     * @param parentId The parent_id
     */
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    /**
     * @return The topParentId
     */
    public String getTopParentId() {
        return topParentId;
    }

    /**
     * @param topParentId The top_parent_id
     */
    public void setTopParentId(String topParentId) {
        this.topParentId = topParentId;
    }

    /**
     * @return The producerId
     */
    public Integer getProducerId() {
        return producerId;
    }

    /**
     * @param producerId The producer_id
     */
    public void setProducerId(Integer producerId) {
        this.producerId = producerId;
    }

    /**
     * @return The rank
     */
    public Double getRank() {
        return rank;
    }

    /**
     * @param rank The rank
     */
    public void setRank(Double rank) {
        this.rank = rank;
    }

    /**
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The part
     */
    public String getPart() {
        return part;
    }

    /**
     * @param part The part
     */
    public void setPart(String part) {
        this.part = part;
    }


    @Override
    public int compareTo(CommodityR o) {
        if (Integer.parseInt(this.getPrice()) < Integer.parseInt(o.getPrice())) {
            return -1;
        } else if (Integer.parseInt(this.getPrice()) == Integer.parseInt(o.getPrice())) {
            return 0;
        } else if (Integer.parseInt(this.getPrice()) > Integer.parseInt(o.getPrice())) {
            return 1;
        }
        return 0;
    }
}