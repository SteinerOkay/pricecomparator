package com.steiner.comparator.stores.citrus;

import com.steiner.comparator.stores.citrus.entity.CommodityC;
import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Query;

import java.util.ArrayList;

public interface CitrusService {
    @GET("/search-atocomplete.php")
    ArrayList<CommodityC> findGoods(
            @Query("term") String request
    );

    @GET("/search-atocomplete.php")
    Response findGoodsRaw(
            @Query("term") String request
    );
}
