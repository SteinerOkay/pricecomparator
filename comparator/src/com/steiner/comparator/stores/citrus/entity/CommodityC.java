package com.steiner.comparator.stores.citrus.entity;

import com.google.gson.annotations.Expose;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class CommodityC implements Comparable<CommodityC> {

    @Expose
    private String value;
    @Expose
    private String ID;
    @Expose
    private String image;
    @Expose
    private String link;
    @Expose
    private String price;

    /**
     * @return The value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value The value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return The ID
     */
    public String getID() {
        return ID;
    }

    /**
     * @param ID The ID
     */
    public void setID(String ID) {
        this.ID = ID;
    }

    /**
     * @return The image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * @return The link
     */
    public String getLink() {
        return link;
    }

    /**
     * @param link The link
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     * @return The price
     */
    public String getPrice() {
        return price;
    }

    /**
     * @param price The price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Good{");
        sb.append("value='").append(value).append('\'');
        sb.append(", ID='").append(ID).append('\'');
        sb.append(", image='").append(image).append('\'');
        sb.append(", link='").append(link).append('\'');
        sb.append(", price='").append(price).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public int compareTo(CommodityC o) {
        if (Integer.parseInt(this.getPrice()) < Integer.parseInt(o.getPrice())) {
            return -1;
        } else if (Integer.parseInt(this.getPrice()) == Integer.parseInt(o.getPrice())) {
            return 0;
        } else if (Integer.parseInt(this.getPrice()) > Integer.parseInt(o.getPrice())) {
            return 1;
        }
        return 0;
    }
}