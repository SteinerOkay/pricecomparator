package com.steiner.comparator.stores.citrus;

import com.steiner.comparator.Config;
import com.steiner.comparator.SearchEngine;
import com.steiner.comparator.stores.BaseStore;
import com.steiner.comparator.stores.citrus.entity.CommodityC;

import java.util.ArrayList;
import java.util.Collections;


public class Citrus extends BaseStore {

    public Citrus(String request) {
        super(Config.CITRUS_API_URL, request);
    }

    public CitrusService service() {
        return getRestAdapter().create(CitrusService.class);
    }

    @Override
    public void run() {
        super.run();
        this.setIsDebug(Config.IS_DEBUG);
        CitrusService citrusService = this.service();
        ArrayList<CommodityC> commodities = citrusService.findGoods(this.request);

        if (commodities.size() > 0) {
            for (int i = 0; i < commodities.size(); i++) {
                commodities.get(i).setPrice(commodities.get(i).getPrice().replace(" ", "").replace("грн.", ""));
            }
            Collections.sort(commodities);

            System.out.println("Топ Citrus: ");
            if (commodities.size() > 3) {
                for (int i = 0; i < 3; i++) {
                    System.out.println("Citrus: " + commodities.get(i).getPrice() + " -- " + commodities.get(i).getValue());
                }
            } else {
                for (CommodityC commodity : commodities) {
                    System.out.println("Citrus: " + commodity.getPrice() + " -- " + commodity.getValue());
                }
            }
            SearchEngine.citrusLow = commodities.get(0);
            /*for (Commodity commodity : commodities) {
                System.out.println("Citrus: " + commodity.getPrice() + " -- " + commodity.getValue());
            }*/
        }
    }
}
