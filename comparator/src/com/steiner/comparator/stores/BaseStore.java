package com.steiner.comparator.stores;

import com.steiner.comparator.Utils;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

public abstract class BaseStore extends Thread {
    protected String apiUrl;
    protected String request;
    protected RestAdapter restAdapter;
    protected boolean isDebug;

    public BaseStore(String apiUrl, String request) {
        this.apiUrl = apiUrl;
        this.request = request;
    }

    public BaseStore setIsDebug(boolean isDebug) {
        this.isDebug = isDebug;
        if (restAdapter != null) {
            restAdapter.setLogLevel(isDebug ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE);
        }
        return this;
    }

    public RestAdapter getRestAdapter() {
        if (restAdapter == null) {
            RestAdapter.Builder builder = new RestAdapter.Builder();
            builder.setEndpoint(apiUrl);
            builder.setClient(new OkClient(Utils.createOkHttpClient()));
            if (isDebug) {
                builder.setLogLevel(RestAdapter.LogLevel.FULL);
            }
            restAdapter = builder.build();
        }
        return restAdapter;
    }

}
