package com.steiner.comparator;

import java.util.ArrayList;

public class Runner {
    public static void main(String[] args) {
        ArrayList<String> queries = new ArrayList<String>();
        queries.add("iPhone 6 128GB S1i1lve1r1");
        queries.add("iPhone 6 128GB Silver");
        queries.add("Samsung UE48H4200");
        queries.add("Fitbit Charge L HR");

        for (String query : queries) {
            new SearchEngine(query).search();
            System.out.println("--------------------------------------------");
        }
    }
}
