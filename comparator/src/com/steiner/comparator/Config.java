package com.steiner.comparator;


public class Config {
    public static final boolean IS_DEBUG = false;
    public static final boolean USE_PROXY = false;
    public static final String PROXY_URL = "192.168.100.2";
    public static final int PROXY_PORT = 3128;
    public static final String ROZETKA_API_URL = "http://rozetka.com.ua";
    public static final String CITRUS_API_URL = "http://www.citrus.ua";
}
