package com.steiner.comparator;

import com.squareup.okhttp.OkHttpClient;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.concurrent.TimeUnit;

public class Utils {
    public static final int CONNECT_TIMEOUT_MILLIS = 15000; // 15 seconds
    public static final int READ_TIMEOUT_MILLIS = 20000; // 20 seconds

    public static OkHttpClient createOkHttpClient() {
        OkHttpClient client = new OkHttpClient();
        if (Config.USE_PROXY) {
            client.setProxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(Config.PROXY_URL, Config.PROXY_PORT)));
        }
        client.setConnectTimeout(CONNECT_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);
        client.setReadTimeout(READ_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);
        client.setRetryOnConnectionFailure(true);

        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        client.setCookieHandler(cookieManager);

        return client;
    }

}