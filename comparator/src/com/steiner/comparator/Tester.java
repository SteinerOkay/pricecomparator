package com.steiner.comparator;

import com.steiner.comparator.stores.citrus.Citrus;
import com.steiner.comparator.stores.citrus.CitrusService;
import com.steiner.comparator.stores.rozetka.Rozetka;
import com.steiner.comparator.stores.rozetka.RozetkaService;

public class Tester {
    public static void main(String[] args) {
        Tester tester = new Tester();
        tester.testCitrus();
        tester.testRozetka();
    }

    public void testRozetka() {
        Rozetka rozetka = new Rozetka("");
        rozetka.setIsDebug(Config.IS_DEBUG);
        RozetkaService rozetkaService = rozetka.service();
        System.out.println(rozetkaService.findGoods(Rozetka.makeInput("text=Apple%20iPhone%206%20128GB&suggest=1")).toString());
    }

    public void testCitrus() {
        Citrus citrus = new Citrus("");
        citrus.setIsDebug(Config.IS_DEBUG);
        CitrusService citrusService = citrus.service();
        System.out.println(citrusService.findGoods("iPhone 6"));
    }
}
