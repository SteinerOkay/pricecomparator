package com.steiner.comparator;

import com.steiner.comparator.stores.citrus.Citrus;
import com.steiner.comparator.stores.citrus.entity.CommodityC;
import com.steiner.comparator.stores.rozetka.Rozetka;
import com.steiner.comparator.stores.rozetka.entity.CommodityR;

public class SearchEngine extends Thread {
    private String searchQuery;
    public static CommodityR rozetkaLow = null;
    public static CommodityC citrusLow = null;

    public SearchEngine(String searchQuery) {
        this.searchQuery = searchQuery;
    }

    public void search() {
        System.out.println("Вывод по запросу\" " + this.searchQuery + "\":");
        Rozetka tRozetka = new Rozetka(this.searchQuery);
        tRozetka.start();

        Citrus tCitrus = new Citrus(this.searchQuery);
        tCitrus.start();

        try {
            tRozetka.join();
            tCitrus.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        this.analyze();
    }

    public void analyze() {
        if ((rozetkaLow == null) && (citrusLow == null)) {
            System.out.println("В 2 магазинах товар " + this.searchQuery + " отсутствует");
        } else if (citrusLow == null) {
            System.out.println("В магазине Citrus товар " + this.searchQuery + " отсутствует");
            System.out.println("В магазине Rozetka товар " + rozetkaLow.getTitle() + " имеет цену " + rozetkaLow.getPrice() + " грн.");
        } else if (rozetkaLow == null) {
            System.out.println("В магазине Rozetka товар " + this.searchQuery + " отсутствует");
            System.out.println("В магазине Citrus товар " + citrusLow.getValue() + " имеет цену " + citrusLow.getPrice() + " грн.");
        } else if (Integer.parseInt(citrusLow.getPrice()) < Integer.parseInt(rozetkaLow.getPrice())) {
            System.out.println("В магазине Citrus товар " + citrusLow.getValue() + " дешевле и имеет цену " + citrusLow.getPrice() + " грн.");
        } else {
            System.out.println("В магазине Rozetka товар " + rozetkaLow.getTitle() + " дешевле и имеет цену " + rozetkaLow.getPrice() + " грн.");
        }
        rozetkaLow = null;
        citrusLow = null;
    }

    @Override
    public void run() {
        super.run();
        this.search();
    }
}
